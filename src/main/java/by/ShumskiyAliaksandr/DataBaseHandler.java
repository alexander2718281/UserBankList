package by.ShumskiyAliaksandr;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class DataBaseHandler extends Config{
    Connection dbConnection;

    public Connection getDbConnection() throws SQLException, ClassNotFoundException {
        String ConnectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?verifyServerCertificate=false"+
                "&useSSL=false"+
                "&requireSSL=false"+
                "&useLegacyDatetimeCode=false"+
                "&amp"+
                "&serverTimezone=UTC";
        dbConnection = DriverManager.getConnection(ConnectionString, dbUser, dbPass);
        return dbConnection;
    }

    public void returnUserList() throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnection().createStatement();
        String sql = "SELECT * FROM " + Const.USER_TABLE;
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()){
            int ID = rs.getInt(1);
            String name = rs.getString(2);
            String sureName = rs.getString(3);
            System.out.print(ID + " " + name + " " + sureName);
            System.out.println();
        }
    }
    public void returnUser(int IDuser) throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnection().createStatement();
        String sql = "SELECT * FROM " + Const.USER_TABLE + " WHERE " + Const.USERS_ID + "=" + IDuser;
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()){
            int ID = rs.getInt(1);
            String name = rs.getString(2);
            String sureName = rs.getString(3);
            System.out.print(ID + " " + name + " " + sureName);
            System.out.println();
        }
    }

    public int returnAccontsSumm () throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnection().createStatement();
        String sql = "SELECT " + Const.ACCOUNT + " FROM " + Const.ACCOUNT_TABLE;
        ResultSet rs = statement.executeQuery(sql);
        int summ = 0;
        while (rs.next()){
            int account = rs.getInt(Const.ACCOUNT);
            summ += account;
        }
        return summ;
    }

    public String returnRichestUser() throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnection().createStatement();
        Map<Integer, Integer> summOfAccounts = new HashMap<Integer, Integer>();
        String sql = "SELECT " + Const.USERS_ID + " FROM " + Const.USER_TABLE;
        ResultSet rs = statement.executeQuery(sql);
        while (rs.next()){
            int ID = rs.getInt(Const.USERS_ID);
            summOfAccounts.put(ID, 0);
        }
        String sq2 = "SELECT " + Const.ACCOUNT + "," + Const.USERS_ID + " FROM " + Const.ACCOUNT_TABLE;
        rs = statement.executeQuery(sq2);
        while (rs.next()){
            int ID = rs.getInt(Const.USERS_ID);
            int account = rs.getInt(Const.ACCOUNT);
            int summ;
            summ = summOfAccounts.get(ID);
            summ += account;
            summOfAccounts.put(ID, summ);
        }
        int max = 0;
        int maxID = 0;
        for (Map.Entry<Integer, Integer> summs: summOfAccounts.entrySet() ) {
            if (summs.getValue() >= max){
                maxID = summs.getKey();
            }
        }
        String sq3 = "SELECT * FROM " + Const.USER_TABLE + " WHERE " + Const.USERS_ID + "=" + maxID;
        rs = statement.executeQuery(sq3);
        String richestUser = null;
        while (rs.next()){
            int ID = rs.getInt(1);
            String name = rs.getString(2);
            String sureName = rs.getString(3);
            richestUser = "Richest is " + ID + " " + name + " " + sureName;
        }
            return richestUser;
        }

    }
