package by.ShumskiyAliaksandr;

public class Const {
    public static final String USER_TABLE = "user";
    public static final String USERS_ID = "userID";
    public static final String USERS_NAME = "name";
    public static final String USERS_SURENAME = "sureName";
    public static final String ACCOUNT_TABLE = "account";
    public static final String ACCOUNT_ID = "accountID";
    public static final String ACCOUNT = "account";
}
