package by.ShumskiyAliaksandr;

import java.sql.SQLException;

public class App
{
    public static void main( String[] args ){
        DataBaseHandler db = new DataBaseHandler();
        try {
            db.returnUserList();
            db.returnUser(5);
            System.out.println(db.returnAccontsSumm());
            System.out.println(db.returnRichestUser());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
